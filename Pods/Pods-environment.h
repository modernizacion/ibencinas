
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 2
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 2
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 2
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// MKMapView-ZoomLevel
#define COCOAPODS_POD_AVAILABLE_MKMapView_ZoomLevel
#define COCOAPODS_VERSION_MAJOR_MKMapView_ZoomLevel 1
#define COCOAPODS_VERSION_MINOR_MKMapView_ZoomLevel 1
#define COCOAPODS_VERSION_PATCH_MKMapView_ZoomLevel 0

// OCMapView
#define COCOAPODS_POD_AVAILABLE_OCMapView
#define COCOAPODS_VERSION_MAJOR_OCMapView 0
#define COCOAPODS_VERSION_MINOR_OCMapView 0
#define COCOAPODS_VERSION_PATCH_OCMapView 1

// SSToolkit
#define COCOAPODS_POD_AVAILABLE_SSToolkit
#define COCOAPODS_VERSION_MAJOR_SSToolkit 1
#define COCOAPODS_VERSION_MINOR_SSToolkit 0
#define COCOAPODS_VERSION_PATCH_SSToolkit 4

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 0
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// TestFlightSDK
#define COCOAPODS_POD_AVAILABLE_TestFlightSDK
#define COCOAPODS_VERSION_MAJOR_TestFlightSDK 2
#define COCOAPODS_VERSION_MINOR_TestFlightSDK 2
#define COCOAPODS_VERSION_PATCH_TestFlightSDK 1

