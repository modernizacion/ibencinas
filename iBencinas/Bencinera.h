//
//  Bencinera.h
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Bencinera : NSObject

@property(nonatomic,strong) NSString *nombre;
@property(nonatomic,strong) NSString *direccion;
@property(nonatomic,strong) NSString *horario;
@property(nonatomic,strong) CLLocation *ubicacion;
@property(nonatomic,assign) int gasolina93;
@property(nonatomic,assign) int gasolina95;
@property(nonatomic,assign) int gasolina97;
@property(nonatomic,assign) int diesel;
@property(nonatomic,assign) int kerosene;
@property(nonatomic ,readonly) UIImage *logoImage;

@end
