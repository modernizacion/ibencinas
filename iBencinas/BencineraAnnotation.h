//
//  BencineraAnnotation.h
//  iBencinas
//
//  Created by Nicolas Silva on 31-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Bencinera.h"
#import <OCMapView/OCGrouping.h>

@interface BencineraAnnotation : MKPointAnnotation <OCGrouping>

@property(nonatomic,strong)Bencinera *bencinera;
@property(nonatomic,strong) NSString *groupTag;

@end
