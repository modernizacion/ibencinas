//
//  ListadoBencinerasCell.h
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bencinera.h"

@interface ListadoBencinerasCell : UITableViewCell

@property (nonatomic,strong) Bencinera* bencinera;

@property (weak, nonatomic) IBOutlet UILabel *precioLabel;
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *direccionLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanciaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end
