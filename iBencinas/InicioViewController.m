//
//  InicioViewController.m
//  iBencinas
//
//  Created by Nicolas Silva on 27-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "InicioViewController.h"
#import "SectorTable.h"
#import "ContainerViewController.h"
#import <UIColor+SSToolkitAdditions.h>

@interface InicioViewController ()

@property(nonatomic,strong) NSArray *searchResults;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
- (IBAction)cercanasButtonTouched:(id)sender;

@end

@implementation InicioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //[self styleSearchBar];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden=YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cercanasButtonTouched:(id)sender{
    [self performSegueWithIdentifier:@"bencinasCercanas" sender:sender];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    
    self.searchResults=[SectorTable searchByNombre:searchString];

    
    return YES;
    
}

-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    self.navigationController.navigationBar.hidden=YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self performSegueWithIdentifier:@"bencinasPorDireccion" sender:searchBar];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary *result=self.searchResults[indexPath.row];
    cell.textLabel.text=result[@"nombre"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"bencinasPorDireccion" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"bencinasPorDireccion"]) {
        ContainerViewController *vc=segue.destinationViewController;
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *cell=(UITableViewCell *)sender;
            vc.direccion=cell.textLabel.text;
        }else if ([sender isKindOfClass:[UISearchBar class]]) {
            UISearchBar *searchBar=(UISearchBar *)sender;
            vc.direccion=searchBar.text;
        }
        
    }
}

@end
