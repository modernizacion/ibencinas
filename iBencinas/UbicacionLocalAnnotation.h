//
//  UbicacionLocalAnnotation.h
//  iBencinas
//
//  Created by Nicolas Silva on 19-02-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <OCMapView/OCGrouping.h>

@interface UbicacionLocalAnnotation : MKPointAnnotation <OCGrouping>

@property(nonatomic,strong) NSString *groupTag;

@end
