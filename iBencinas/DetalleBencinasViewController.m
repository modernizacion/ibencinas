//
//  DetalleBencinasViewController.m
//  iBencinas
//
//  Created by Nicolas Silva on 31-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "DetalleBencinasViewController.h"
#import <UIColor+SSToolkitAdditions.h>
#import "ListadoPreciosCell.h"
#import "BencineraAnnotation.h"
#import "BencineraAnnotationView.h"

@interface DetalleBencinasViewController ()
- (IBAction)comoLlegarTouched:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *horarioLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *direccionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DetalleBencinasViewController

-(void)viewDidAppear:(BOOL)animated{
    //Se ejecuta luego del autolayout.
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.mapView.frame.size.height - 2.0, self.mapView.frame.size.width, 2.0f);
    bottomBorder.backgroundColor = [UIColor colorWithHex:@"358B9F"].CGColor;
    [self.mapView.layer addSublayer:bottomBorder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title=self.bencinera.nombre.capitalizedString;
    
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(self.bencinera.ubicacion.coordinate.latitude+0.001, self.bencinera.ubicacion.coordinate.longitude), 1000, 1000) animated:NO];
    
    BencineraAnnotation *annotation=[[BencineraAnnotation alloc] init];
    annotation.bencinera=self.bencinera;
    annotation.coordinate=self.bencinera.ubicacion.coordinate;
    [self.mapView addAnnotation:annotation];
    
    self.nombreLabel.text=self.bencinera.nombre.capitalizedString;
    self.direccionLabel.text=self.bencinera.direccion;
    self.horarioLabel.text=[NSString stringWithFormat:@"Abierto %@",self.bencinera.horario];
    self.logoImageView.image=self.bencinera.logoImage;
    
    self.tableView.layer.borderColor=[UIColor colorWithHex:@"ACACAC"].CGColor;
    self.tableView.layer.borderWidth=2.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    ListadoPreciosCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row==0) {
        cell.tipoLabel.text=@"Gas 93";
        cell.precioLabel.text=self.bencinera.gasolina93?[NSString stringWithFormat:@"$%d",self.bencinera.gasolina93]:@"No tiene";
    }else if (indexPath.row==1) {
        cell.tipoLabel.text=@"Gas 95";
        cell.precioLabel.text=self.bencinera.gasolina95?[NSString stringWithFormat:@"$%d",self.bencinera.gasolina95]:@"No tiene";
    }else if (indexPath.row==2) {
        cell.tipoLabel.text=@"Gas 97";
        cell.precioLabel.text=self.bencinera.gasolina97?[NSString stringWithFormat:@"$%d",self.bencinera.gasolina97]:@"No tiene";
    }else if (indexPath.row==3) {
        cell.tipoLabel.text=@"Diesel";
        cell.precioLabel.text=self.bencinera.diesel?[NSString stringWithFormat:@"$%d",self.bencinera.diesel]:@"No tiene";
    }else if (indexPath.row==4) {
        cell.tipoLabel.text=@"Kerosene";
        cell.precioLabel.text=self.bencinera.kerosene?[NSString stringWithFormat:@"$%d",self.bencinera.kerosene]:@"No tiene";
    }
    
    return cell;
}

- (IBAction)comoLlegarTouched:(id)sender {
    //MKPlacemark *farmaciaPlacemark=[[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake([self.farmacia[@"lat"] doubleValue], [self.farmacia[@"lng"] doubleValue] ) addressDictionary:nil];
    //farmaciaPlacemark.name=@"Hola";
    
    MKMapItem *bencineraItem=[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.bencinera.ubicacion.coordinate addressDictionary:nil]];
    
    bencineraItem.name=self.bencinera.nombre;
    //CLLocation* fromLocation = from.placemark.location;
    
    // Create a region centered on the starting point with a 10km span
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(fromLocation.coordinate, 10000, 10000);
    
    // Open the item in Maps, specifying the map region to display.
    [MKMapItem openMapsWithItems:@[[MKMapItem mapItemForCurrentLocation],bencineraItem] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[BencineraAnnotation class]]) {
        BencineraAnnotation *bencineraAnnotation=(BencineraAnnotation *)annotation;
        
        BencineraAnnotationView *view=[[BencineraAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"BencineraAnnotationView"];
        
        //Actualizamos el logo
        view.image=bencineraAnnotation.bencinera.logoImage;
        
        //Actualizamos la distancia
         float distanciaKilometros=[self.ubicacion distanceFromLocation:bencineraAnnotation.bencinera.ubicacion] / 1000;
         NSNumberFormatter *nf=[[NSNumberFormatter alloc] init];
         nf.numberStyle=NSNumberFormatterDecimalStyle;
         nf.minimumFractionDigits=0;
         nf.maximumFractionDigits=1;
         view.text=[NSString stringWithFormat:@"%@km",[nf stringFromNumber:@(distanciaKilometros)]];

        
        return view;
    }
    
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
