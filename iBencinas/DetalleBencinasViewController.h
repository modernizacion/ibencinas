//
//  DetalleBencinasViewController.h
//  iBencinas
//
//  Created by Nicolas Silva on 31-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Bencinera.h"

@interface DetalleBencinasViewController : UIViewController <MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)Bencinera *bencinera;
@property(nonatomic,strong)CLLocation *ubicacion;

@end
