//
//  BencineraTable.m
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "BencineraTable.h"
#import "Bencinera.h"
#import "config.h"

@implementation BencineraTable


+(void)findAllWithBlock:(void (^)(NSArray *, NSError *))block cachePolicy:(NSURLRequestCachePolicy)cachePolicy{
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLRequest *request=[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://api.cne.cl/api/listaInformacion/",API_CNE_TOKEN]] cachePolicy:cachePolicy timeoutInterval:30];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        NSMutableArray *bencineras=[[NSMutableArray alloc] init];
        NSDictionary *json=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        for (NSDictionary *f in json[@"data"]) {
            Bencinera *bencinera=[[Bencinera alloc] init];
            bencinera.nombre=f[@"nombre_distribuidor"];
            bencinera.ubicacion=[[CLLocation alloc] initWithLatitude:[f[@"latitud"] doubleValue] longitude:[f[@"longitud"] doubleValue]];
            bencinera.direccion=[NSString stringWithFormat:@"%@ %@, %@",f[@"direccion_calle"],f[@"direccion_numero"],f[@"nombre_comuna"]];
            bencinera.horario=f[@"horario_atencion"];
            bencinera.gasolina93=[f[@"precio_por_combustible"][@"gasolina_93"] intValue];
            bencinera.gasolina95=[f[@"precio_por_combustible"][@"gasolina_95"] intValue];
            bencinera.gasolina97=[f[@"precio_por_combustible"][@"gasolina_97"] intValue];
            bencinera.diesel=[f[@"precio_por_combustible"][@"petroleo_diesel"] intValue];
            bencinera.kerosene=[f[@"precio_por_combustible"][@"kerosene"] intValue];
            
            [bencineras addObject:bencinera];
        }
        if(block){  //Como el block del NSURLSession corre en un background thread, tenemos que lanzar este block al main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                block(bencineras,error);
            });
        }
        
    }] resume];
    
    
}

+(void)findAllWithBlock:(void (^)(NSArray *, NSError *))block{
    [self findAllWithBlock:block cachePolicy:NSURLRequestUseProtocolCachePolicy];
}

@end
