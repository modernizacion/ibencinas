//
//  ListadoPreciosCell.h
//  iBencinas
//
//  Created by Nicolas Silva on 20-01-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListadoPreciosCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tipoLabel;
@property (weak, nonatomic) IBOutlet UILabel *precioLabel;

@end
