//
//  BencineraAnnotationView.m
//  iBencinas
//
//  Created by Nicolas Silva on 17-01-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import "BencineraAnnotationView.h"


@implementation BencineraAnnotationView
@synthesize image=_image, text=_text;

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Set the frame size to the appropriate values.
        CGRect  myFrame = self.frame;
        myFrame.size.width = 39;
        myFrame.size.height = 44;
        self.frame = myFrame;
        
        // The opaque property is YES by default. Setting it to
        // NO allows map content to show through any unrendered
        // parts of your view.
        self.opaque = NO;
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* fillColor = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1];
    UIColor* color = [UIColor colorWithRed: 0.373 green: 0.745 blue: 0.835 alpha: 1];
    
    //// Image Declarations
    UIImage* bencinerashell = self.image;
    
    //// Abstracted Attributes
    NSString* labelContent = self.text;
    
    
    //// marco Drawing
    UIBezierPath* marcoPath = [UIBezierPath bezierPath];
    [marcoPath moveToPoint: CGPointMake(38, 3.3)];
    [marcoPath addLineToPoint: CGPointMake(38, 34.72)];
    [marcoPath addCurveToPoint: CGPointMake(36.13, 36.51) controlPoint1: CGPointMake(38, 35.71) controlPoint2: CGPointMake(37.16, 36.51)];
    [marcoPath addLineToPoint: CGPointMake(23.96, 36.51)];
    [marcoPath addLineToPoint: CGPointMake(23.96, 41)];
    [marcoPath addLineToPoint: CGPointMake(21.62, 36.51)];
    [marcoPath addLineToPoint: CGPointMake(3.37, 36.51)];
    [marcoPath addCurveToPoint: CGPointMake(1.5, 34.72) controlPoint1: CGPointMake(2.34, 36.51) controlPoint2: CGPointMake(1.5, 35.71)];
    [marcoPath addLineToPoint: CGPointMake(1.5, 3.3)];
    [marcoPath addCurveToPoint: CGPointMake(3.37, 1.5) controlPoint1: CGPointMake(1.5, 2.3) controlPoint2: CGPointMake(2.34, 1.5)];
    [marcoPath addLineToPoint: CGPointMake(36.13, 1.5)];
    [marcoPath addCurveToPoint: CGPointMake(38, 3.3) controlPoint1: CGPointMake(37.16, 1.5) controlPoint2: CGPointMake(38, 2.3)];
    [marcoPath closePath];
    [fillColor setFill];
    [marcoPath fill];
    [color setStroke];
    marcoPath.lineWidth = 1.5;
    [marcoPath stroke];
    
    
    //// label Drawing
    CGRect labelRect = CGRectMake(0, 20, 39, 17);
    //[color setFill];
    //[labelContent drawInRect: labelRect withFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 11] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment=NSTextAlignmentCenter;
    [labelContent drawInRect:labelRect withAttributes:@{NSFontAttributeName:[UIFont fontWithName: @"HelveticaNeue-Bold" size: 11],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName:color}];
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(9.5, 3.5, 20, 19)];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    [bencinerashell drawInRect: rectanglePath.bounds];
    CGContextRestoreGState(context);
    

    
}

-(void)setText:(NSString *)text{
    _text=text;
    
    [self setNeedsDisplay];
}

-(void)setImage:(UIImage *)image{
    _image=image;
    
    [self setNeedsDisplay];
}

@end
