//
//  BencineraTable.h
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BencineraTable : NSObject

+(void)findAllWithBlock:(void(^)(NSArray *, NSError *))block cachePolicy: (NSURLRequestCachePolicy)cachePolicy;
+(void)findAllWithBlock:(void(^)(NSArray *, NSError *))block;

@end
