//
//  MSSearchBar.m
//  iBencinas
//
//  Created by Nicolas Silva on 16-01-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import "MSSearchBar.h"
#import <UIColor+SSToolkitAdditions.h>

@interface MSSearchBar ()

-(void)commonInit;

@end

@implementation MSSearchBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self =[super initWithCoder:aDecoder];
    
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self setImage:[UIImage imageNamed:@"search-icon.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self setSearchFieldBackgroundImage:[UIImage imageNamed:@"searchbar-bg.png"] forState:UIControlStateNormal];
    [self setSearchTextPositionAdjustment:UIOffsetMake(5, 0)];
    
    for (UIView *subView in self.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                searchBarTextField.layer.borderWidth=1.0;
                searchBarTextField.layer.borderColor=[[UIColor whiteColor] CGColor];
                searchBarTextField.layer.cornerRadius=2.5;
                
                //set font color here
                searchBarTextField.attributedPlaceholder=[[NSAttributedString alloc] initWithString:searchBarTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"53EFFF"]}];
                
                searchBarTextField.font=[UIFont fontWithName:@"HelveticaNeue-Light" size:18];
                
                searchBarTextField.textColor=[UIColor colorWithHex:@"53EFFF"];
                
                break;
            }
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
