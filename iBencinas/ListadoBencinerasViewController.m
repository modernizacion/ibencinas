//
//  ListadoBencinerasViewController.m
//  iBencinas
//
//  Created by Nicolas Silva on 24-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "ListadoBencinerasViewController.h"
#import "BencineraTable.h"
#import "Bencinera.h"
#import "ListadoBencinerasCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ContainerViewController.h"
#import "DetalleBencinasViewController.h"
#import <UIColor+SSToolkitAdditions.h>

@interface ListadoBencinerasViewController ()
-(void)reloadData;  //Se redesplegan los datos (Se vuelven a calcular distancias, etc)
- (IBAction)orderSegmentedControlValueChanged:(id)sender;
- (IBAction)typeSegmentedControlValueChanged:(id)sender;
-(void)refreshControlChanged;

@property(nonatomic,strong) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic, strong) NSArray *bencineras;
@property (nonatomic, strong) NSArray *filteredBencineras;
@property (weak, nonatomic) IBOutlet UISegmentedControl *orderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeSegmentedControl;

@end

@implementation ListadoBencinerasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (!self.refreshControl)
        self.refreshControl=[[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshControlChanged) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    self.tableView.contentInset=UIEdgeInsetsMake(0, 0, self.toolbar.frame.size.height, 0);
    self.tableView.scrollIndicatorInsets=UIEdgeInsetsMake(0, 0, self.toolbar.frame.size.height, 0);
    
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    [[NSNotificationCenter defaultCenter] addObserverForName:@"locationChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
        [self reloadData];
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"dataChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
        [self.refreshControl endRefreshing];
        [self reloadData];
    }];
    
    
    UIFont *font=[UIFont fontWithName:@"HelveticaNeue" size:11];
    [self.typeSegmentedControl setBackgroundImage:[UIImage imageNamed:@"tab-normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.typeSegmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHex:@"676767"], NSFontAttributeName: font} forState:UIControlStateNormal];
    [self.typeSegmentedControl setBackgroundImage:[UIImage imageNamed:@"tab-selected.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.typeSegmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHex:@"1D1D1D"], NSFontAttributeName: font} forState:UIControlStateSelected];
    [self.typeSegmentedControl setDividerImage:[UIImage imageNamed:@"tab-divider.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.typeSegmentedControl setDividerImage:[UIImage imageNamed:@"tab-divider-blank.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.typeSegmentedControl setDividerImage:[UIImage imageNamed:@"tab-divider-blank.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    
    [vc refreshData];
    
}

-(void)reloadData{
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    
    if (!vc.bencineras || !vc.location) {
        return;
    }
    
    NSPredicate *predicate=[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        BOOL shouldAppear=YES;
        
        Bencinera *bencinera = (Bencinera *)evaluatedObject;
        if ([vc.location distanceFromLocation:bencinera.ubicacion] > 5000) {
            shouldAppear = NO;
        }
        
        if (vc.selectedBencineraType==GASOLINA93) {
            if (bencinera.gasolina93==0) shouldAppear = NO;
        }else if (vc.selectedBencineraType==GASOLINA95) {
            if (bencinera.gasolina95==0) shouldAppear = NO;
        }else if (vc.selectedBencineraType==GASOLINA97) {
            if (bencinera.gasolina97==0) shouldAppear = NO;
        }else if (vc.selectedBencineraType==DIESEL) {
            if (bencinera.diesel==0) shouldAppear = NO;
        }else if (vc.selectedBencineraType==KEROSENE) {
            if (bencinera.kerosene==0) shouldAppear = NO;
        }
        
        return shouldAppear;
    }];
    self.filteredBencineras=[vc.bencineras filteredArrayUsingPredicate:predicate];
    
    
    self.filteredBencineras = [self.filteredBencineras sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        Bencinera *bencineraA = (Bencinera *)a;
        Bencinera *bencineraB = (Bencinera *)b;
        
        if (self.orderSegmentedControl.selectedSegmentIndex==0) {
            CLLocationDistance distancia1=[vc.location distanceFromLocation:[bencineraA ubicacion]];
            CLLocationDistance distancia2=[vc.location distanceFromLocation:[bencineraB ubicacion]];
            
            return distancia1 > distancia2;
        }else{
            if (vc.selectedBencineraType==GASOLINA93) {
                return bencineraA.gasolina93 > bencineraB.gasolina93;
            }else if (vc.selectedBencineraType==GASOLINA95) {
                return bencineraA.gasolina95 > bencineraB.gasolina95;
            }else if (vc.selectedBencineraType==GASOLINA97) {
                return bencineraA.gasolina97 > bencineraB.gasolina97;
            }else if (vc.selectedBencineraType==DIESEL) {
                return bencineraA.diesel > bencineraB.diesel;
            }else if (vc.selectedBencineraType==KEROSENE){
                return bencineraA.kerosene > bencineraB.kerosene;
            }
            
            return NO;
            
            
        }
        
    }];
    
    [self.tableView reloadData];
}

- (IBAction)orderSegmentedControlValueChanged:(id)sender {
    [self reloadData];
}

- (IBAction)typeSegmentedControlValueChanged:(id)sender {
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    vc.selectedBencineraType=(int)self.typeSegmentedControl.selectedSegmentIndex;
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.filteredBencineras.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListadoBencinerasCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.bencinera=self.filteredBencineras[indexPath.row];
    cell.nombreLabel.text=cell.bencinera.nombre.capitalizedString;
    cell.direccionLabel.text=cell.bencinera.direccion;
    
    if (self.orderSegmentedControl.selectedSegmentIndex==0) {
        cell.precioLabel.textColor=[UIColor colorWithHex:@"1D1D1D"];
        cell.distanciaLabel.textColor=[UIColor colorWithHex:@"5FBDD5"];
    }else{
        cell.precioLabel.textColor=[UIColor colorWithHex:@"5FBDD5"];
        cell.distanciaLabel.textColor=[UIColor colorWithHex:@"1D1D1D"];
    }
    
    if (self.typeSegmentedControl.selectedSegmentIndex==GASOLINA93) {
        cell.precioLabel.text=[NSString stringWithFormat:@"$%d",cell.bencinera.gasolina93];
    }else if (self.typeSegmentedControl.selectedSegmentIndex==GASOLINA95) {
        cell.precioLabel.text=[NSString stringWithFormat:@"$%d",cell.bencinera.gasolina95];
    }else if (self.typeSegmentedControl.selectedSegmentIndex==GASOLINA97) {
        cell.precioLabel.text=[NSString stringWithFormat:@"$%d",cell.bencinera.gasolina97];
    }else if (self.typeSegmentedControl.selectedSegmentIndex==DIESEL) {
        cell.precioLabel.text=[NSString stringWithFormat:@"$%d",cell.bencinera.diesel];
    }else if (self.typeSegmentedControl.selectedSegmentIndex==KEROSENE) {
        cell.precioLabel.text=[NSString stringWithFormat:@"$%d",cell.bencinera.kerosene];
    }
    
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    float distanciaKilometros=[vc.location distanceFromLocation:cell.bencinera.ubicacion] / 1000;
    NSNumberFormatter *nf=[[NSNumberFormatter alloc] init];
    nf.numberStyle=NSNumberFormatterDecimalStyle;
    nf.minimumFractionDigits=0;
    nf.maximumFractionDigits=1;
    cell.distanciaLabel.text=[NSString stringWithFormat:@"%@km",[nf stringFromNumber:@(distanciaKilometros)]];
    
    UIImage *logoImage=[UIImage imageNamed:[NSString stringWithFormat:@"bencinera-%@.png",cell.bencinera.nombre.lowercaseString]];
    if (logoImage) {
        cell.logoImageView.image=logoImage;
    }else{
        cell.logoImageView.image=[UIImage imageNamed:@"bencinera-unknown.png"];
    }
    
    return cell;
}


-(void)refreshControlChanged{
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    [vc refreshDataWithCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ContainerViewController *cvc=(ContainerViewController *)self.parentViewController;
    DetalleBencinasViewController *vc=(DetalleBencinasViewController *)segue.destinationViewController;
    ListadoBencinerasCell *cell=(ListadoBencinerasCell *)sender;
    vc.bencinera=cell.bencinera;
    vc.ubicacion=cvc.location;
    
}

@end
