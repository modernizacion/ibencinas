//
//  ListadoPreciosCell.m
//  iBencinas
//
//  Created by Nicolas Silva on 20-01-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import "ListadoPreciosCell.h"

@implementation ListadoPreciosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
