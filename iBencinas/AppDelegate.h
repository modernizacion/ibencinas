//
//  AppDelegate.h
//  iBencinas
//
//  Created by Nicolas Silva on 24-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
