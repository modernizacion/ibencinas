//
//  MapaBencinasViewController.m
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "MapaBencinasViewController.h"
#import "ContainerViewController.h"
#import "Bencinera.h"
#import <OCMapView/OCMapView.h>
#import "ClusterAnnotationView.h"
#import "BencineraAnnotation.h"
#import "UbicacionLocalAnnotation.h"
#import "BencineraAnnotationView.h"
#import "DetalleBencinasViewController.h"
#import <UIColor+SSToolkitAdditions.h>
#import <MKMapView+ZoomLevel.h>

@interface MapaBencinasViewController ()

-(void)reloadData;
- (IBAction)locateButtonTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *tipoLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *locateButton;
@property (weak, nonatomic) IBOutlet OCMapView *mapView;
@property(nonatomic,assign) BOOL panToUserLocation;
@property (nonatomic, strong) CLLocation *userLocation;
@end

@implementation MapaBencinasViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.mapView.clusterSize=0.1;
    self.mapView.clusterByGroupTag=YES;
    
    self.locateButton.tintColor=[UIColor whiteColor];
    
    self.infoView.layer.cornerRadius=5.0;
    self.infoView.layer.opacity=0.9;
    self.infoView.layer.borderWidth=1.0;
    self.infoView.layer.borderColor=[UIColor colorWithHex:@"5EBDD3"].CGColor;
    self.infoView.layer.backgroundColor=[UIColor whiteColor].CGColor;
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    //[[NSNotificationCenter defaultCenter] addObserverForName:@"locationChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
    //    [self reloadData];
    //}];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"configChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
        [self reloadData];
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"dataChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
        [self reloadData];
    }];
    
    //Centramos el mapa
    if (vc.location)
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(vc.location.coordinate, 2000, 2000) animated:NO];
    else
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(-33.44288, -70.65383), 20000, 20000) animated:NO];
    
    
    
    
    
    
    if (vc.direccion) {
        self.locateButton.image=[UIImage imageNamed:@"tabbar-cercanas.png"];
        self.panToUserLocation=NO;
        [[NSNotificationCenter defaultCenter] addObserverForName:@"locationChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
            [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(vc.location.coordinate, 2000, 2000) animated:YES];
        }];
    }else{
        self.locateButton.image=[UIImage imageNamed:@"tabbar-cercanas-locate.png"];
        self.panToUserLocation=YES;
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"dataChanged" object:vc queue:nil usingBlock:^(NSNotification *note) {
        [self reloadData];
    }];
    
    
    
    [self reloadData];
    
    
}

-(void)reloadData{
    ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
    
    if (vc.selectedBencineraType==GASOLINA93) {
        self.tipoLabel.text=@"Gasolina 93";
    }else if (vc.selectedBencineraType==GASOLINA95) {
        self.tipoLabel.text=@"Gasolina 95";
    }else if (vc.selectedBencineraType==GASOLINA97) {
        self.tipoLabel.text=@"Gasolina 97";
    }else if (vc.selectedBencineraType==DIESEL) {
        self.tipoLabel.text=@"Diesel";
    }else if (vc.selectedBencineraType==KEROSENE) {
        self.tipoLabel.text=@"Kerosene";
    }
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    //Agregamos las bencineras al mapa
    NSMutableArray *annotations=[[NSMutableArray alloc] init];
    for (Bencinera *b in vc.bencineras) {
        if ((vc.selectedBencineraType==GASOLINA93 && b.gasolina93) ||
            (vc.selectedBencineraType==GASOLINA95 && b.gasolina95) ||
            (vc.selectedBencineraType==GASOLINA97 && b.gasolina97) ||
            (vc.selectedBencineraType==DIESEL && b.diesel) ||
            (vc.selectedBencineraType==KEROSENE && b.kerosene)) {
            BencineraAnnotation *annotation=[[BencineraAnnotation alloc] init];
            annotation.bencinera=b;
            annotation.groupTag=@"bencinera";
            annotation.title=b.nombre;
            annotation.subtitle=b.direccion;
            annotation.coordinate=b.ubicacion.coordinate;
            
            [annotations addObject:annotation];
        }
        
    }
    [self.mapView addAnnotations:annotations];
    
    //Agregamos la ubicacion inicial al mapa
    if (vc.direccion) {
        UbicacionLocalAnnotation *annotation=[[UbicacionLocalAnnotation alloc] init];
        annotation.groupTag=@"ubicacion-local";
        annotation.coordinate=vc.location.coordinate;
        annotation.title=vc.direccion;
        [self.mapView addAnnotation:annotation];
    }
    
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    if (!self.userLocation || [self.userLocation distanceFromLocation:userLocation.location]>1000) {    //Se requiere actualizar localizacion si no se ha actualizado, o si ha variado mucho.
        if (self.panToUserLocation) {     //Movemos el mapa si es la primera vez, y si es que esta la opcion de que el mapa te siga
            [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 2000, 2000) animated:YES];
        }
        self.userLocation=userLocation.location;    //Actualizamos la localizacion
    }
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    // If it's the user location, just return nil.
    if ([annotation isMemberOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isMemberOfClass:[OCAnnotation class]]) {
        OCAnnotation *clusterAnnotation=(OCAnnotation *)annotation;
        
        // create your custom cluster annotationView here!
        ClusterAnnotationView *annotationView = (ClusterAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterAnnotationView"];
        
        if (!annotationView)
        {
            // If an existing pin view was not available, create one.
            annotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterAnnotationView"];
            
            
        }
        else
            annotationView.annotation = clusterAnnotation;
        
        annotationView.color=[UIColor colorWithRed:94.0/255.0 green:189.0/255.0 blue:211.0/255.0 alpha:0.8];

        
        annotationView.numero=(int)clusterAnnotation.annotationsInCluster.count;
        
        return annotationView;
    }
    
    // Handle any custom annotations.
    if ([annotation isMemberOfClass:[BencineraAnnotation class]])
    {
        BencineraAnnotation *bencineraAnnotation=(BencineraAnnotation *)annotation;
        
        // Try to dequeue an existing pin view first.
        BencineraAnnotationView*    pinView = (BencineraAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"BencineraAnnotationView"];
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[BencineraAnnotationView alloc] initWithAnnotation:bencineraAnnotation
                                                      reuseIdentifier:@"BencineraAnnotationView"];
            
            
            
            
            
            pinView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.
            
            UIButton* rightButton = [UIButton buttonWithType:
                                     UIButtonTypeDetailDisclosure];
            rightButton.tintColor=[UIColor colorWithHex:@"5EBDD3"];
            
            pinView.rightCalloutAccessoryView = rightButton;
            
        }
        else
            pinView.annotation = bencineraAnnotation;
        
        //Actualizamos la distancia
        /*
        ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
        float distanciaKilometros=[vc.location distanceFromLocation:bencineraAnnotation.bencinera.ubicacion] / 1000;
        NSNumberFormatter *nf=[[NSNumberFormatter alloc] init];
        nf.numberStyle=NSNumberFormatterDecimalStyle;
        nf.minimumFractionDigits=0;
        nf.maximumFractionDigits=1;
        pinView.text=[NSString stringWithFormat:@"%@km",[nf stringFromNumber:@(distanciaKilometros)]];
         */
        
        //Actualizamos los precios
        ContainerViewController *vc=(ContainerViewController *)self.parentViewController;
        if (vc.selectedBencineraType==GASOLINA93) {
            pinView.text=[NSString stringWithFormat:@"$%d",bencineraAnnotation.bencinera.gasolina93];
        }else if (vc.selectedBencineraType==GASOLINA95){
            pinView.text=[NSString stringWithFormat:@"$%d",bencineraAnnotation.bencinera.gasolina95];
        }else if (vc.selectedBencineraType==GASOLINA97){
            pinView.text=[NSString stringWithFormat:@"$%d",bencineraAnnotation.bencinera.gasolina97];
        }else if (vc.selectedBencineraType==DIESEL){
            pinView.text=[NSString stringWithFormat:@"$%d",bencineraAnnotation.bencinera.diesel];
        }else if (vc.selectedBencineraType==KEROSENE){
            pinView.text=[NSString stringWithFormat:@"$%d",bencineraAnnotation.bencinera.kerosene];
        }
        
        //Actualizamos el logo
        UIImage *bencineraImage=[UIImage imageNamed:[NSString stringWithFormat:@"bencinera-%@.png",bencineraAnnotation.bencinera.nombre.lowercaseString]];
        if (!bencineraImage)
            bencineraImage=[UIImage imageNamed:@"bencinera-unknown.png"];
        pinView.image=bencineraImage;
        
        
        return pinView;
    }
    
    if ([annotation isMemberOfClass:[UbicacionLocalAnnotation class]]) {
        MKPinAnnotationView *pinView=(MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
        if (!pinView) {
            pinView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotationView"];
            pinView.canShowCallout=YES;
        }else{
            pinView.annotation=annotation;
        }
        
        return pinView;
    }
    
    return nil;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    [self performSegueWithIdentifier:@"bencinaDetalle" sender:view.annotation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)locateButtonTouched:(id)sender {
    if (self.panToUserLocation) {
        self.locateButton.image=[UIImage imageNamed:@"tabbar-cercanas.png"];
        self.panToUserLocation=NO;
    }else{
        self.locateButton.image=[UIImage imageNamed:@"tabbar-cercanas-locate.png"];
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.userLocation.coordinate, 2000, 2000) animated:YES];
        self.panToUserLocation=YES;
    }
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated{
    NSUInteger zoomLevel=[self.mapView zoomLevel];
    if (zoomLevel > 16) {
        self.mapView.clusteringEnabled=NO;
    }else{
        self.mapView.clusteringEnabled=YES;
    }
    
    [self.mapView doClustering];
 
}

-(void)didDragMap:(UIGestureRecognizer*)gestureRecognizer{
    if (self.panToUserLocation) {
        self.locateButton.image=[UIImage imageNamed:@"tabbar-cercanas.png"];
        self.panToUserLocation=NO;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ContainerViewController *cvc=(ContainerViewController *)self.parentViewController;
    DetalleBencinasViewController *vc = (DetalleBencinasViewController *)segue.destinationViewController;
    BencineraAnnotation *annotation=(BencineraAnnotation *)sender;
    vc.bencinera=annotation.bencinera;
    vc.ubicacion=cvc.location;
}
@end
