//
//  ClusterAnnotationView.m
//  iTransantiago
//
//  Created by Nicolas Silva on 20-02-13.
//
//

#import "ClusterAnnotationView.h"

@implementation ClusterAnnotationView
@synthesize numero;

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Set the frame size to the appropriate values.
        CGRect  myFrame = self.frame;
        myFrame.size.width = 40;
        myFrame.size.height = 40;
        self.frame = myFrame;
        
        // The opaque property is YES by default. Setting it to
        // NO allows map content to show through any unrendered
        // parts of your view.
        self.opaque = NO;
        
        self.color=[UIColor colorWithRed:107.0/255.0 green:194.0/255.0 blue:61.0/255.0 alpha:0.8];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(ctx, rect);
    CGContextSetFillColor(ctx, CGColorGetComponents([[self color] CGColor]));
    CGContextFillPath(ctx);
    
    
    NSString *texto=[NSString stringWithFormat:@"%d",[self numero]];    
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment=NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode=NSLineBreakByClipping;
    
    [texto drawInRect:CGRectMake(rect.origin.x, rect.origin.y+10, rect.size.width, rect.size.height) withAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14],NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
}

-(void)setNumero:(int)n{
    numero=n;
    [self setNeedsDisplay];
}

-(int)numero{
    return numero;
}


@end
