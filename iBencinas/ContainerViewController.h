//
//  BencinasViewController.h
//  iBencinas
//
//  Created by Nicolas Silva on 24-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#define GASOLINA93  0
#define GASOLINA95  1
#define GASOLINA97  2
#define DIESEL      3
#define KEROSENE    4

@interface ContainerViewController : UIViewController <CLLocationManagerDelegate>

@property (nonatomic, strong) NSString *direccion;
@property (nonatomic, strong, readonly) NSArray *bencineras;
@property (nonatomic, strong, readonly) CLLocation *location;
@property (nonatomic, assign) int selectedBencineraType;

-(void)refreshData;
-(void)refreshDataWithCachePolicy:(NSURLRequestCachePolicy)cachePolicy;

@end
