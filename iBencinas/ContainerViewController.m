//
//  BencinasViewController.m
//  iBencinas
//
//  Created by Nicolas Silva on 24-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "ContainerViewController.h"
#import "BencineraTable.h"
#import <SVProgressHUD/SVProgressHUD.h>


@interface ContainerViewController ()
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (weak,nonatomic) UIViewController *selectedViewController;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

- (IBAction)segmentedControlValueChanged:(id)sender;
@end

@implementation ContainerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    if (self.direccion==nil) {
        if (!self.locationManager)
            self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 100;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            [self.locationManager requestWhenInUseAuthorization];
        
        [SVProgressHUD showWithStatus:@"Localizando dirección"];
        [self.locationManager startUpdatingLocation];
    }else{
        [SVProgressHUD showWithStatus:@"Localizando dirección"];
        CLGeocoder *geocoder=[[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:self.direccion inRegion:nil completionHandler:^(NSArray *placemarks, NSError *error) {
            [SVProgressHUD popActivity];
            if (!error) {
                CLPlacemark *placemark=placemarks[0];
                _location=placemark.location;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"locationChanged" object:self];
            }else{
                [[[UIAlertView alloc] initWithTitle:@"No encontrado" message:@"Dirección no encontrada" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
    }
    
    
    UIViewController *listadoViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"listadoBencineras"];
    UIViewController *mapaViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"mapaBencineras"];
    
    [self addChildViewController:listadoViewController];
    [listadoViewController didMoveToParentViewController:self];
    [self addChildViewController:mapaViewController];
    [mapaViewController didMoveToParentViewController:self];
    
    [self.containerView addSubview:listadoViewController.view];
    self.selectedViewController=listadoViewController;
    
    self.segmentedControl.tintColor=[UIColor whiteColor];   //Fix de bug en IB. No queda seteado el tint color.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden=NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedControlValueChanged:(id)sender {
    [self transitionFromViewController: self.selectedViewController toViewController: self.childViewControllers[self.segmentedControl.selectedSegmentIndex]
                              duration: 0 options:0
                            animations:nil
                            completion:^(BOOL finished) {
                                self.selectedViewController=self.childViewControllers[self.segmentedControl.selectedSegmentIndex];
                            }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (_location==nil) {   //Significa que es la primera vez que se esta localizando. Por lo tanto, existira un progresshud que hay que matar.
        [SVProgressHUD popActivity];
    }
    
    
    _location=[locations lastObject];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"locationChanged" object:self];
}

-(void)refreshDataWithCachePolicy:(NSURLRequestCachePolicy)cachePolicy{
    [SVProgressHUD showWithStatus:@"Cargando"];
    [BencineraTable findAllWithBlock:^(NSArray *bencineras, NSError *error) {
        [SVProgressHUD popActivity];
        if(error){
            //[SVProgressHUD showSuccessWithStatus:@"Error"];
        }else {
            
            _bencineras=bencineras;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dataChanged" object:self];
            
            //[SVProgressHUD showSuccessWithStatus:@"Completado"];
        }
    } cachePolicy:cachePolicy];
}

-(void)refreshData{
    [self refreshDataWithCachePolicy:NSURLRequestUseProtocolCachePolicy];
}

-(void)setSelectedBencineraType:(int)selectedBencineraType{
    _selectedBencineraType=selectedBencineraType;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"configChanged" object:self];
}
@end
