//
//  BencineraAnnotationView.h
//  iBencinas
//
//  Created by Nicolas Silva on 17-01-14.
//  Copyright (c) 2014 Nicolas Silva. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface BencineraAnnotationView : MKAnnotationView

@property(nonatomic,strong) NSString *text;
@property(nonatomic,strong) UIImage *image;

@end
