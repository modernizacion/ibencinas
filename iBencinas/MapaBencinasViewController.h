//
//  MapaBencinasViewController.h
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapaBencinasViewController : UIViewController <MKMapViewDelegate,UIGestureRecognizerDelegate>



@end
