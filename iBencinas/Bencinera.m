//
//  Bencinera.m
//  iBencinas
//
//  Created by Nicolas Silva on 26-12-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "Bencinera.h"

@implementation Bencinera


-(UIImage *)logoImage{
    UIImage *logoImage=[UIImage imageNamed:[NSString stringWithFormat:@"bencinera-%@.png",self.nombre.lowercaseString]];
    if (!logoImage) {
        logoImage=[UIImage imageNamed:@"bencinera-unknown.png"];
    }
    
    return logoImage;
}

@end
